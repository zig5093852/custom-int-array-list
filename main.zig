const std = @import("std");
const Allocator = std.mem.Allocator;
const testing = std.testing;

const CustomIntArrayList = @import("Cust_int_array_list.zig");
const ArrIntList = CustomIntArrayList.CustomIntArrayList;

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const alloc = arena.allocator();
    var myIntArray = try ArrIntList.init(alloc, 4);
    try myIntArray.add(12);
    std.debug.print("{}\n", .{myIntArray});
}
