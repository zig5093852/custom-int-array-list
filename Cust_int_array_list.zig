const std = @import("std");
const Allocator = std.mem.Allocator;
const testing = std.testing;

const CustIntArrListOutOfBoundsError = error{ IndexOutOfBounds, EmptyList };

pub const CustomIntArrayList = struct {
    list: []i32,
    length: u16,
    allocator: Allocator,

    pub fn init(allocator: Allocator, capacity: u16) !CustomIntArrayList {
        return .{
            .list = try allocator.alloc(i32, @as(usize, capacity)),
            .length = 0,
            .allocator = allocator,
        };
    }

    pub fn deinit(self: *const CustomIntArrayList) void {
        self.*.allocator.free(self.list);
    }

    pub fn isEmpty(self: *const CustomIntArrayList) bool {
        return self.*.length == 0;
    }

    pub fn add(self: *CustomIntArrayList, value: i32) !void {
        if (self.*.length == self.*.list.len) {
            const oldList = self.*.list;
            defer self.*.allocator.free(oldList);

            self.*.list = try self.*.allocator.alloc(i32, self.*.length * 2);

            for (0..self.*.length) |i| {
                self.*.list[i] = oldList[i];
            }
        }

        self.*.list[@as(usize, self.length)] = value;
        self.*.length += 1;
    }

    pub fn pop(self: *CustomIntArrayList) void {
        if (self.*.length == 0) {
            return;
        }

        self.*.list[self.*.length] = undefined;
        self.*.length -= 1;
    }

    pub fn get(self: *CustomIntArrayList, index: usize) CustIntArrListOutOfBoundsError!i32 {
        if (self.*.length == 0) {
            return CustIntArrListOutOfBoundsError.EmptyList;
        }

        if (index >= self.*.length) {
            return CustIntArrListOutOfBoundsError.IndexOutOfBounds;
        }

        return self.*.list[index];
    }

    pub fn insert(self: *CustomIntArrayList, index: usize, value: i32) anyerror!void {
        if (index != 0 and self.*.length == 0) {
            return CustIntArrListOutOfBoundsError.EmptyList;
        }

        if (index >= self.*.length) {
            return CustIntArrListOutOfBoundsError.IndexOutOfBounds;
        }

        if (self.*.length == self.*.list.len) {
            const oldList = self.*.list;
            defer self.*.allocator.free(oldList);

            self.*.list = try self.*.allocator.alloc(i32, self.*.length * 2);

            for (0..index) |i| {
                self.*.list[i] = oldList[i];
            }
            self.*.list[index] = value;
            self.*.length += 1;
            for (index + 1..self.*.length) |i| {
                self.*.list[i] = oldList[i - 1];
            }
        } else {
            var i = self.*.length;
            while (i > index) : (i -= 1) {
                self.*.list[i] = self.*.list[i - 1];
            }

            self.*.list[index] = value;
            self.*.length += 1;
        }
    }

    pub fn remove(self: *CustomIntArrayList, index: usize) !void {
        if (self.*.length == 0) {
            return CustIntArrListOutOfBoundsError.EmptyList;
        }

        if (index >= self.*.length) {
            return CustIntArrListOutOfBoundsError.IndexOutOfBounds;
        }

        for (index..self.*.length) |i| {
            if (index == self.*.length - 1) {
                self.*.list[index] = undefined;
                break;
            }

            self.*.list[i] = self.*.list[i + 1];
        }

        self.*.length-=1;
    }
};

test "Should init and deinit CustomIntArray without mem leak" {
    const arr = try CustomIntArrayList.init(testing.allocator, 8);
    defer arr.deinit();
}

test "Should init CustomIntArray properly" {
    const capacity = 8;
    const arr = try CustomIntArrayList.init(testing.allocator, capacity);
    defer arr.deinit();

    try testing.expect(arr.length == 0);
    try testing.expect(arr.list.len == capacity);
}

test "isEmpty function should work correctly" {
    const arr = try CustomIntArrayList.init(testing.allocator, 8);
    defer arr.deinit();
    try testing.expectEqual(true, arr.isEmpty());
}

test "Should add element to list" {
    var arr = try CustomIntArrayList.init(testing.allocator, 8);
    defer arr.deinit();

    try arr.add(69);

    try testing.expectEqual(false, arr.isEmpty());
    try testing.expectEqual(@as(u16, 1), arr.length);
}

test "Should resize list when trying to add to full list" {
    var arr = try CustomIntArrayList.init(testing.allocator, 2);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(17);
    try arr.add(666);

    try testing.expectEqual(false, arr.isEmpty());
    try testing.expectEqual(@as(u16, 3), arr.length);
    try testing.expectEqual(@as(usize, 4), arr.list.len);

    try testing.expectEqual(@as(i32, 69), arr.list[0]);
    try testing.expectEqual(@as(i32, 17), arr.list[1]);
    try testing.expectEqual(@as(i32, 666), arr.list[2]);
}

test "Pop function should work" {
    var arr = try CustomIntArrayList.init(testing.allocator, 2);
    defer arr.deinit();

    try arr.add(69);
    try testing.expectEqual(false, arr.isEmpty());

    arr.pop();
    try testing.expectEqual(true, arr.isEmpty());
}

test "When popping from empty list, nothing should happen" {
    var arr = try CustomIntArrayList.init(testing.allocator, 2);
    defer arr.deinit();

    try testing.expectEqual(true, arr.isEmpty());
    arr.pop(); // expect no error of any sort
}

test "Should get element by index" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(17);
    try arr.add(666);

    try testing.expectEqual(@as(i32, 69), try arr.get(0));
    try testing.expectEqual(@as(i32, 17), try arr.get(1));
    try testing.expectEqual(@as(i32, 666), try arr.get(2));
}

test "Should get CustIntArrListOutOfBoundsError" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try testing.expectError(CustIntArrListOutOfBoundsError.EmptyList, arr.get(0));

    try arr.add(69);
    try arr.add(17);
    try arr.add(666);

    try testing.expectError(CustIntArrListOutOfBoundsError.IndexOutOfBounds, arr.get(3));
    try testing.expectError(CustIntArrListOutOfBoundsError.IndexOutOfBounds, arr.get(5));
}

test "Should insert value in the middle" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(666);
    try testing.expectEqual(@as(usize, 2), arr.length);

    try arr.insert(1, 17);
    try testing.expectEqual(@as(usize, 3), arr.length);
    try testing.expectEqual(@as(i32, 69), arr.list[0]);
    try testing.expectEqual(@as(i32, 17), arr.list[1]);
    try testing.expectEqual(@as(i32, 666), arr.list[2]);
}

test "Should insert value at the beggining" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(666);
    try testing.expectEqual(@as(usize, 2), arr.length);

    try arr.insert(0, 17);
    try testing.expectEqual(@as(usize, 3), arr.length);
    try testing.expectEqual(@as(i32, 17), arr.list[0]);
    try testing.expectEqual(@as(i32, 69), arr.list[1]);
    try testing.expectEqual(@as(i32, 666), arr.list[2]);
}

test "Expect OutOfBounds error when inserting at the end" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(666);
    try testing.expectError(CustIntArrListOutOfBoundsError.IndexOutOfBounds, arr.insert(2, 17));
}

test "Expect EmptyList error when inserting at the beggining of empty list" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try testing.expectError(CustIntArrListOutOfBoundsError.IndexOutOfBounds, arr.insert(0, 17));
}

test "Should resize when trying to insert into full list" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(666);
    try arr.add(17);
    try arr.add(22);
    try testing.expectEqual(@as(usize, 4), arr.length);
    try testing.expectEqual(@as(usize, 4), arr.list.len);

    try arr.insert(2, 18);
    try testing.expectEqual(@as(usize, 5), arr.length);
    try testing.expectEqual(@as(usize, 8), arr.list.len);
    try testing.expectEqual(@as(i32, 69), arr.list[0]);
    try testing.expectEqual(@as(i32, 666), arr.list[1]);
    try testing.expectEqual(@as(i32, 18), arr.list[2]);
    try testing.expectEqual(@as(i32, 17), arr.list[3]);
    try testing.expectEqual(@as(i32, 22), arr.list[4]);
}

test "Should remove middle item" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(666);
    try arr.add(17);
    try testing.expectEqual(@as(usize, 3), arr.length);

    try arr.remove(1);
    try testing.expectEqual(@as(usize, 2), arr.length);
    try testing.expectEqual(@as(i32, 69), arr.list[0]);
    try testing.expectEqual(@as(i32, 17), arr.list[1]);
}

test "Should remove first item" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(666);
    try arr.add(17);
    try testing.expectEqual(@as(usize, 3), arr.length);

    try arr.remove(0);
    try testing.expectEqual(@as(usize, 2), arr.length);
    try testing.expectEqual(@as(i32, 666), arr.list[0]);
    try testing.expectEqual(@as(i32, 17), arr.list[1]);
}

test "Should remove last item" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(666);
    try arr.add(17);
    try testing.expectEqual(@as(usize, 3), arr.length);

    try arr.remove(2);
    try testing.expectEqual(@as(usize, 2), arr.length);
    try testing.expectEqual(@as(i32, 69), arr.list[0]);
    try testing.expectEqual(@as(i32, 666), arr.list[1]);
}

test "Expect OutOfBounds error when removing" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try arr.add(69);
    try arr.add(666);

    try testing.expectError(CustIntArrListOutOfBoundsError.IndexOutOfBounds, arr.remove(2));
    try testing.expectError(CustIntArrListOutOfBoundsError.IndexOutOfBounds, arr.remove(4));
}

test "Expect EmptyList error when removing" {
    var arr = try CustomIntArrayList.init(testing.allocator, 4);
    defer arr.deinit();

    try testing.expectError(CustIntArrListOutOfBoundsError.EmptyList, arr.remove(0));
}
